import math

a = 1.05509847590646
b = 1.38973494774893
c = -1.4625430235504

delta = (b ** 2) - 4 * a * c
x1 = (-b - math.sqrt(delta)) / (2 * a)
x2 = (-b + math.sqrt(delta)) / (2 * a)
print('x1 vale: {:.5f}, x2 vale: {:.5f}'.format(x1, x2))
