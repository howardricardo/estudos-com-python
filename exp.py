import math

n = 0
sum1 = 0
sum2 = 0
sum3 = 0
x = 0.2718198091748186

while n <= 3:
    sum1 = sum1 + math.pow(x, n) / math.factorial(n)
    sum2 = sum2 + math.pow(-1, n) / math.factorial(2 * n + 1) * (math.pow(x, 2 * n + 1))
    sum3 = sum3 + math.factorial(2 * n) / math.pow(4, n) * math.pow(math.factorial(n), 2) * (2 * n + 1) * math.pow(x, 2 * n + 1)
    n += 1
    print('{:.10}'.format(sum1 * sum2 * (math.radians(90) - sum3)))
