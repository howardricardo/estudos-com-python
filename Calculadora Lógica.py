import math
print('Essa é uma calculadora simples. Digite um número conforme a tabela: ')
print(30 * '-')
print('Soma: 1. ')
print('Subtração: 2. ')
print('Multiplicação: 3.')
print('Divisão: 4.')
print(30 * '-')
operador = input('Qual operação você quer fazer? ')

soma = []
subtracao = []
multiplicacao = []
divisao = []
n = 2

while operador.isalpha() or int(operador) > 4 or int(operador) < 0:
    operador = input('Digite um número correspondendo a tabela: ')
operador = int(operador)
if operador == 1:
    so = input('Digite o 1° número: ')
    while so != 'somar':
        while so.isalpha():
            so = input('Somente números: ')
        so = int(so)
        soma.append(so)
        print(f'Sua lista para somar é: {soma}')
        print('Para remover o último item da soma soma, digite "remover"')
        while so != 'somar':
            so = input(f'Digite o {n}° número: ')
            while so.isalpha() and so != 'somar':
                so = input('Somente números: ')
                if so.isalpha() is False:
                    so = int(so)
            if so == 'remover':
                soma.pop()
                print(f'Sua lista para somar é: {soma}')
            if so.isalpha() is False:
                so = int(so)
            soma.append(so)
            if so == 'somar':
                soma.pop()
            print(f'Sua lista para somar é: {soma}')
            n += 1
        print(f'A soma dos números: {soma} é: {sum(soma)}')
