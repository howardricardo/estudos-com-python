"""
Covid19:
    Versão Alfa 1.0:

        Inicialmente eu tive essa ideia mais para testar meus conhecimentos sobre python que, ainda, são bem razos. Eu
    tive essa ideia e fui firme nela até o fim. Primeiro eu queria um código que apenas a pessoa colocasse os dados do
    país dela e já mostrasse o número de contaminados (segundo dados confirmados) em relação ao número da população. O
    código era bem simples na sua versão 1.0 alfa: A pessoa colocava o número de pessoas que desejava analisar, o número
    de casos e o número de mortes. Desde o início eu utilizei como base um estudo feito na Inglaterra que diz: Se um gr-
    upo de pessoas em sociedade está em pandemia de vírus que pode não trazer sintomas e que a consequência é o isola-
    mento social que, naturalmente, as pessoa tendem a não seguir corretamente ou necessariamente, o número de casos
    confirmados deve ser multiplicado por 7, 8 ou 9. Em casos de países subdesenvolvidos, multiplica-se por 9 ou por 10.
        Até aí, beleza. O cálculo é dividir o número de casos pela população, aí multiplica por 100 e vai dar a porcen-
    tagem do valor de número de casos. Dividindo o número de mortes pelo número de casos, depois multiplicando por 100,
    temos a taxa de letalidade do vírus.
        Mas sempre penso que o usuário pode desafiar meu código e fazer coisas por diversão... Testar meu código e colo-
    car números sem sentido, letras ou qualquer coisa que podem resultar numa divisão por 0, gerar um loop infinito ou
    algo assim. Pensei muito sobre isso e cheguei numa frase que pode definir meu sentimento em relação aos usuários:

              >print('Usuários são como água: sempre acham um caminho ou jeito de dar um erro no seu código')<

        Eu fiz uma piada com o comando "print()" do Python. Desculpe.

    Versão Beta 2.0:

        Pensando assim, eu comecei a pensar... E se ele colocasse população sendo 0? E se ele quisesse analisar os dados
    de um país específico e não souber quais são os dados populacionais de tal país? E foi aí que eu tive uma ideia que,
    modéstia a parte, foi tão louca quanto brilhante. Usei um dicionário do python e listei 200 países com suas res-
    pectivas populações. Sim. Um por um. Desde aí, a pessoa só precisava saber o nome do país e os dados em relação ao
    Corona Vírus do país desejado. Isso resolve o problema de ele colocar população sendo 0, já que não existe país com
    nenhuma pessoa.

    Versão Beta 2.1:

        Ainda pensando em usuários, eu acabei concluindo que provavelmente a pessoa pode não querer usar meu banco de
    dados e querer analisar um grupo específico de pessoas, sejam eles dados mais atuais (já que meu banco de dados é
    referente ao ano de 2019), ou sejam dados mais pessoais (como dados de sua cidade, ou um grupo específico de pes-
    soas). Então coloquei a opção de a pessoa escolher se quer usar meu banco de dados ou não. Desde aí já comecei a ter
    ideias para que resolvessem uma séries de problemas. O usuário que, como eu disse, é como água, sempre vai achar um
    jeito de dar um erro. Inicialmente as respostas poderiam ser apenas "Sim" ou "Não", desta forma: com letras maiúscu-
    las e tio no 'a' do "Não". Qualquer outra coisa, por exemplo, "sim" ou "não" ou "nao", daria erro.
        Aí vem a resolução dos problemas, porque o problema das letras maiúsculas se aplicavam também na hora de digitar
    o nome dos países: Se você digitasse, por exemplo, "brasil", dava erro. Então atualizei o código usando a função
    que aumenta a primeira letra de todas as palavras: .title(). Assim, todas as palavras que o usuário colocasse, daria
    uma palavra cuja a primeira letra fosse maiúscula. Exemplo:

                    >>> pais = 'brasil'
                    >>> pais.title()
                    >>> print(pais)
                    ...'Brasil'

        Isso também se aplica ao "sim" e "não". Já a outra questão de acento no 'a' do "não", eu resolvi simplesmente
    adicionando "nao" como uma das opções. Se ele digitasse "sim" ou "Sim" ou "s", ele utilizaria meu banco de dados co-
    mo base. Caso digitasse "Não" ou "Nao" ou "não" ou "nao" ou "n", ele teria que colocar o número de pessoas e todo o
    resto.

    Versão Final:

        Com todos esses ajutes, eu  acabei conseguindo gerar um código satisfatório que gerava gráficos do número de
    casos com seu devido crescimento exponencial e dados fieis condizentes com a realidade. Mas uma coisa me
    intrigava... Sempre que o usuário digitava algo errado, o código travava ali. Então coloquei uma estrutura de repe-
    tição que não deixava o código acabar enquanto o usuário não digitasse dados corretos. Por exemplo:

                    >>> Digite o número de pessoas que deseja analisar: 0
                    >>> O número não pode ser menor ou igual a 0. Digite outro número: -99
                    >>> O número não pode ser menor ou igual a 0. Digite outro número: 0
                    >>> O número não pode ser menor ou igual a 0. Digite outro número: -1
                    >>> O número não pode ser menor ou igual a 0. Digite outro número: 2
                    ...(código segue normalmente)

        Isso também aplicou-se ao "Sim" ou "Não". Que, como eu disse, a pessoa pode digitar: Sim, sim, s, S, Não, não,
    nao, n, N. Por exemplo:

                    >>> Segundo o banco de dados, a população da Argentina é de 44780675. (Dados de 2019)
                    >>> Deseja utilizar estes dados (S/N)?: talvez
                    >>> Resposta esperada: Sim ou Não. Deseja utilizar estes dados?: 95
                    >>> Resposta esperada: Sim ou Não. Deseja utilizar estes dados?: ss
                    >>> Resposta esperada: Sim ou Não. Deseja utilizar estes dados?: acho que sim
                    >>> Resposta esperada: Sim ou Não. Deseja utilizar estes dados?: s
                    ...(código segue normalmente)

        E, por fim, aplicou-se na hora de digitar o nome do país. Por exemplo:

                    >>> Qual o nome do país que deseja analisar?: brasilia
                    >>> Digitação incorreta ou não encontrado no banco de dados. Verifique e digite novamente: Paris
                    >>> Digitação incorreta ou não encontrado no banco de dados. Verifique e digite novamente: Ana
                    >>> Digitação incorreta ou não encontrado no banco de dados. Verifique e digite novamente: Howard
                    >>> Digitação incorreta ou não encontrado no banco de dados. Verifique e digite novamente: Thaíres
                    >>> Digitação incorreta ou não encontrado no banco de dados. Verifique e digite novamente: brasil
                    ...(código segue normalmente. Lembre-se que a função .title() torna 'brasil' em 'Brasil')

    Ideias futuras:

        Bom, com a versão final do meu código, nada me impede de lançar atualizações. Não posso deixar o código automá-
    tico ainda, por exemplo, fazendo a pessoa digitar apenas o nome do país e já ter todos os dados possíveis, porque
    todos os dias os dados em relação ao corona vírus muda. Mas, no futuro, os dados sejam mais constantes e controlá-
    veis e eu não possa fazem algo deste tipo? Com certeza terei muito mais conhecimento sobre programação e terei
    muitas outras ideias brilhantes para agregar ao código.

    Agradecimentos:

        Com carinho, agradeço minha colega de turma e grande amiga Ana, que sempre teve muito a agregar na minha forma
    de pensar e sempre esteve comigo para resolver problemas e falar sobre qualquer assunto leve. Também à minha colega
    de turma Thaíres que sempre deu um jeito de tornar as coisas mais leves quando minhas ideias dentro da minha cabeça
    ainda eram muito turvas e desorganizadas, sem contar o carinho enorme em cada gesto simples que aquecem meu coração.
    E todas as mensagens positivas que recebi de todos que me apoiaram e têm me apoiado durante toda minha caminhada de
    aprendizado. Amo cada um de vocês.
"""
