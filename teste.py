import math
import numpy as np
import matplotlib.pyplot as plt
# -------------------------------------------Função de cada gráfico-----------------------------------------------------
f = lambda x: math.sqrt(1 - math.pow(abs(x) - 1, 2)) + 1
g = lambda x: math.acos(1 - abs(x)) - 2.18

x = np.array(np.linspace(-2, 2, 1000))
y1 = np.array([f(i) for i in x])
y2 = np.array([g(i) for i in x])

plt.plot(x, y1)
plt.plot(x, y2)

plt.ylim(-2.5, 3)
plt.xlim(-2.5, 3)
plt.grid()
plt.show()