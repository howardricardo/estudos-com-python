import math
import numpy as np
import matplotlib.pyplot as plt
# -------------------------------------------Função de cada gráfico-----------------------------------------------------
f = lambda x: math.sqrt(1 - math.pow(abs(x) - 1, 2)) + 1
g = lambda x: math.acos(1 - abs(x)) - 2.18
h = lambda z: (z + 2) / 4
j = lambda w: (w + 2) / 4
t = lambda s: s * 0 + 0.5
p = lambda k: (k + 2) / 4
n = lambda m: m * 0 + 1
a = lambda b: (b + 4.2) / 3.1
c = lambda d: d * 0 + 2
e = lambda f: (f + 19) / 10.5
e1 = lambda f1: (-f1 + 14.5) / 20.8
a2 = lambda b2: (b2 - 6.5) / 3.1
# -------------------------------------------Intervalo de cada função---------------------------------------------------
x = np.array(np.linspace(-2, 2, 1000))
y1 = np.array([f(i) for i in x])
y2 = np.array([g(i) for i in x])

z = np.array(np.linspace(-1.251, -1.25, 1000))
y3 = np.array([h(i) for i in x])

w = np.array(np.linspace(-0.7500001, -0.75, 1000))
y4 = np.array([j(i) for i in x])

s = np.array(np.linspace(-1.25, -0.75, 1000))
y5 = np.array([t(i) for i in s])

k = np.array(np.linspace(1, 1.00000000000000000001, 1000))
y6 = np.array([p(i) for i in x])

m = np.array(np.linspace(0.75, 1.25, 1000))
y7 = np.array([n(i) for i in m])

b = np.array(np.linspace(1.5, 2.5, 1000))
y8 = np.array([a(i) for i in x])

d = np.array(np.linspace(2.25, 2.5, 1000))
y9 = np.array([c(i) for i in x])

f = np.array(np.linspace(2.5001, 2.5, 1000))
y10 = np.array([e(i) for i in x])

f1 = np.array(np.linspace(1.45, 1.55, 1000))
y11 = np.array([e1(i) for i in x])

b2 = np.array(np.linspace(-1.25, -0.25, 1000))
y12 = np.array([a2(i) for i in x])
# -----------------------------------------Plotagem das funções---------------------------------------------------------
plt.plot(x, y1)
plt.plot(x, y2)
plt.plot(z, y3)
plt.plot(w, y4)
plt.plot(s, y5)
plt.plot(k, y6)
plt.plot(m, y7)
plt.plot(b, y8)
plt.plot(d, y9)
plt.plot(f, y10)
plt.plot(f1, y11)
plt.plot(b2, y12)
# --------------------------------Título, limite de x, limite de y e ação do gráfico------------------------------------
plt.title('Coração fofinho')
plt.ylim(-2.5, 3)
plt.xlim(-2.5, 3)
plt.show()
print('Pra você <3')
# ----------------------------------------------------------------------------------------------------------------------
