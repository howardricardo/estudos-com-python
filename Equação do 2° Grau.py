print('Equação do 2° grau!')
# ----------------------------------------------------------------------------------------------------------------------


def trinomio(a, b, c):
    import cmath
    import math
    delta = math.pow(b, 2) - 4 * a * c
    if delta < 0:
        x1 = (-b - cmath.sqrt(delta)) / 2 * a
        x2 = (-b + cmath.sqrt(delta)) / 2 * a
        return 'x1 vale: {:.2f}, x2 vale: {:.2f} e o delta vale: {:.2f}'.format(x1, x2, delta)
    if delta >= 0:
        x1 = (-b - math.sqrt(delta)) / 2 * a
        x2 = (-b + math.sqrt(delta)) / 2 * a
        return 'x1 vale: {:.2f}, x2 vale: {:.2f} e o delta vale: {:.2f}'.format(x1, x2, delta)


# ----------------------------------------------------------------------------------------------------------------------
primeiro = int(input('Digite o valor de a: '))
segundo = int(input('Digite o valor de b: '))
terceiro = int(input('Digite o valor de c: '))

resultado = trinomio(primeiro, segundo, terceiro)
print(resultado)
# ----------------------------------------------------------------------------------------------------------------------
