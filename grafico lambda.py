import math
import numpy as np
import matplotlib.pyplot as plt

f = lambda x: math.pow(x, 30)
#g = lambda x: math.acos(1 - abs(x))

x = np.array(np.linspace(0, 2, 1000))
y1 = np.array([f(i) for i in x])
#y2 = np.array([g(i) for i in x])

plt.plot(x, y1)
#plt.plot(x, y2)

plt.title('Sentimentos pela Thaíres')
plt.grid()
plt.show()
