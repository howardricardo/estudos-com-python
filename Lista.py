print('-------------------------------------------------------------------------')
print('Este é um algorítmo para você montar sua própria lista!')
# ----------------------------------------------------------------------------------------------------------------------
print('-------------------------------------------------------------------------')
nome = input('Qual o nome você quer dar para a sua lista?: ')
print('-------------------------------------------------------------------------')
opcao = input('Você deseja adicionar preço aos itens da sua lista? (Sim/Não)?: ')
opcao = opcao.title()
# ----------------------------------------------------------------------------------------------------------------------
while opcao != 'Sim' and opcao != 'S' and opcao != 'Não' and opcao != 'Nao' and opcao != 'N':
    opcao = input('Resposta esperada: Sim ou Não. Você deseja adicionar preço aos itens da sua lista (Sim/Não)?: ')
    opcao = opcao.title()
# ----------------------------------------------------------------------------------------------------------------------
if opcao == 'Não' or opcao == 'Nao' or opcao == 'N':
    lista = []
    print('-------------------------------------------------------------------------')
    print('>>>Digite o que deseja que entre na lista. Digite "sair" para sair!')
    print('-------------------------------------------------------------------------')
    produto = input('Digite o 1° item: ')
    produto = produto.title()
    lista.append(produto)
    print('-------------------------------------------------------------------------')
    n = 2
# ----------------------------------------------------------------------------------------------------------------------
    while produto != 'Sair':
        produto = input(f'Digite o {n}° item: ')
        produto = produto.title()
        lista.append(produto)
        print('-------------------------------------------------------------------------')
        n += 1
# ----------------------------------------------------------------------------------------------------------------------
    print('-------------------------------------------------------------------------')
    print(f'{nome}:')
# ----------------------------------------------------------------------------------------------------------------------
    lista.remove('Sair')
    m = 1
    for produto in lista:
        print(f'{m}° - {produto}')
        m += 1
    print('-------------------------------------------------------------------------')
if opcao == 'Sim' or opcao == 'S':
    print('-------------------------------------------------------------------------')
    print('>>>Para imprimir a lista, digite "Sair" para o item e "Sair" para o preço do item.')
    lista2 = {}
    print('-------------------------------------------------------------------------')
    a = input('Digite o 1° item: ')
    a = a.title()
    if a == 'Sair':
        lista2.update({'Sair': 'Sair'})
    else:
        b = input('Digite o preço do 1° item: ')
        while b.isnumeric() is False:
            b = input('Digite um valor numérico para o preço do 1° item: ')
        else:
            b = float(b)
            lista2.update({a: b})
    n = 2
# ----------------------------------------------------------------------------------------------------------------------
    while a != 'Sair':
        print('-------------------------------------------------------------------------')
        a = input(f'Digite o {n}° item: ')
        a = a.title()
        if a != 'Sair':
            b = input(f'Digite o preço do {n}° item: ')
            while b.isnumeric() is False:
                b = input(f'Digite um valor numérico para o preço do {n}° item: ')
            else:
                b = float(b)
                lista2.update({a: b})
                n += 1
        elif a == 'Sair':
            lista2.update({'Sair': 'Sair'})
# ----------------------------------------------------------------------------------------------------------------------
    else:
        print('-------------------------------------------------------------------------')
        print('-------------------------------------------------------------------------')
        print(f'{nome}:')
# ----------------------------------------------------------------------------------------------------------------------
        lista2.pop('Sair')
        m = 1
        for a in lista2:
            print('{}° - {}: R${:.2f}'.format(m, a, lista2[a]))
            m += 1
        if lista2:
            print(f'O total a pagar será de: R${int(sum(lista2.values()))},00')
        else:
            print('Lista vazia!')
        print('-------------------------------------------------------------------------')
# ----------------------------------------------------------------------------------------------------------------------
