"""
#---------------------------------------------------------------------------------------------
aula 7:

from tkinter import *
from functools import partial


def bt_click(botao):
    print(botao['text'])


janela = Tk()

bt1 = Button(janela, width=10, text='Botão 1')
bt1['command'] = partial(bt_click, bt1)
bt1.place(x=100, y=100)
bt2 = Button(janela, width=10, text='Botão 2')
bt2['command'] = partial(bt_click, bt2)
bt2.place(x=100, y=130)

janela.geometry('300x300+1200+500')
janela.mainloop()
#---------------------------------------------------------------------------------------------
aula 8:

from tkinter import *


def bt():
    print(ed.get())
    lb['text'] = ed.get()


janela = Tk()

ed = Entry(janela)
ed.place(x=100, y=100)

bt = Button(janela, width=10, text='Ok', command=bt)
bt.place(x=100, y=120)

lb = Label(janela, text='Label')
lb.place(x=100, y=80)

janela.geometry('300x300+1200+500')
janela.mainloop()
#---------------------------------------------------------------------------------------------
aula 9:

from tkinter import *

janela = Tk()


def bt():
    print('Botão clicado')

    if(str(ed1.get()).isnumeric()) and str(ed2.get()).isnumeric():
        num1 = int(ed1.get())
        num2 = int(ed2.get())

        lb['text'] = num1+num2
    else:
        lb['text'] = 'Não numérico'


ed1 = Entry(janela)
ed1.place(x=100, y=100)
ed2 = Entry(janela)
ed2.place(x=100, y=120)

bt = Button(janela, text='Soma', width=10, command=bt)
bt.place(x=100, y=140)

lb = Label(janela, text='Label1')
lb.place(x=100, y=165)

janela.geometry('300x300+1200+500')
janela.mainloop()
#---------------------------------------------------------------------------------------------
aula 10:
from tkinter import *

janela = Tk()

lb1 = Label(janela, text='Label 1', bg='green')
lb2 = Label(janela, text='Label 2', bg='red')
lb3 = Label(janela, text='Label 3', bg='yellow')
lb4 = Label(janela, text='Label 4', bg='blue')

lb1.pack()
lb2.pack()
lb3.pack()
lb4.pack(side=BOTTOM)

janela.geometry('300x300+1200+600')
janela.mainloop()
#---------------------------------------------------------------------------------------------
aula 11:
from tkinter import *

janela = Tk()

lb1 = Label(janela, text='RIGHT', bg='white')
lb2 = Label(janela, text='BOTTOM', bg='white')
lb3 = Label(janela, text='LEFT', bg='white')
lb4 = Label(janela, text='TOP', bg='white')

lb1.pack(side=RIGHT)
lb2.pack(side=BOTTOM)
lb3.pack(side=LEFT)
lb4.pack(side=TOP)

janela['bg'] = 'black'
janela.geometry('300x300+1200+600')
janela.mainloop()
#---------------------------------------------------------------------------------------------
aula 13:
from tkinter import *

janela = Tk()

lb1 = Label(janela, text='horizontal', bg='white')
lb2 = Label(janela, text='', bg='black')
lb3 = Label(janela, text='', bg='black')

lb1.pack(side=TOP, fill=X)
lb2.pack(side=LEFT, fill=Y)
lb3.pack(side=RIGHT, fill=Y)

janela.geometry('300x300+1200+600')
janela.mainloop()
#---------------------------------------------------------------------------------------------
aula 14:
from tkinter import *

janela = Tk()

lb1 = Label(janela, text='linha 1', bg='blue')
lb2 = Label(janela, text='linha 2', bg='yellow')
lb3 = Label(janela, text='linha 3', bg='blue')
lb4 = Label(janela, text='linha 4', bg='yellow')

lb1.pack(side=TOP, fill=BOTH, expand=1)
lb2.pack(side=TOP, fill=BOTH, expand=1)
lb3.pack(side=TOP, fill=BOTH, expand=1)
lb4.pack(side=TOP, fill=BOTH, expand=1)

janela.geometry('300x300+1200+600')
janela.mainloop()
#---------------------------------------------------------------------------------------------
aula 16:
from tkinter import *

janela = Tk()

lb1 = Label(janela, text='label 1')
lb2 = Label(janela, text='label 1')
lb1.grid(row=10, column=10)
lb2.grid(row=20, column=20)

janela.geometry('300x300+1200+600')
janela.mainloop()
#---------------------------------------------------------------------------------------------
aula 17:
from tkinter import *

janela = Tk()
janela['bg'] = 'purple'

lb1 = Label(janela, text='Login: ', bg='purple')
lb2 = Label(janela, text='Senha: ', bg='purple')

ed1 = Entry(janela)
ed2 = Entry(janela)

bt1 = Button(janela, text='Confirmar')

lb1.grid(row=0, column=0)
lb2.grid(row=1, column=0)
ed1.grid(row=0, column=1)
ed2.grid(row=1, column=1)
bt1.grid(row=2, column=1)

janela.geometry('200x100+1200+600')
janela.mainloop()
#---------------------------------------------------------------------------------------------
aula 18:
from tkinter import *

janela = Tk()
janela['bg'] = 'purple'

lb1 = Label(janela, text='ESPAÇO', width='15', height=3, bg='white')
lb2 = Label(janela, text='HORIZONTAL', bg='blue')
lb3 = Label(janela, text='VERTICAL', bg='yellow')


lb1.grid(row=0, column=0)
lb2.grid(row=1, column=0, sticky=E)
lb3.grid(row=0, column=1, sticky=N)

janela.geometry('200x100+1200+600')
janela.mainloop()
#---------------------------------------------------------------------------------------------
aula 19:
from tkinter import *

janela = Tk()
janela['bg'] = 'purple'

lb1 = Label(janela, width=15, height=6, bg='red')
lb2 = Label(janela, width=15, height=6, bg='blue')
lb3 = Label(janela, width=15, height=6, bg='black')
lb4 = Label(janela, width=15, height=6, bg='yellow')

lb5 = Label(janela, height=3, bg='green')
lb6 = Label(janela, width=5, bg='pink')

lb1.grid(row=0, column=0)
lb2.grid(row=1, column=0)
lb3.grid(row=0, column=1)
lb4.grid(row=1, column=1)

lb5.grid(row=2, column=0, columnspan=2, sticky=W+E)
lb6.grid(row=0, column=2, rowspan=2, sticky=N+S)

janela.geometry('500x300+1200+600')
janela.mainloop()
#---------------------------------------------------------------------------------------------
aula 20:

#---------------------------------------------------------------------------------------------
"""